# Transcribing Calls
### powered by Eyeoda

Video call made in ReactJS. Video chat is made for easy communication between an anonymous party and an advisor who is specialized in a specific field.
The transcription server is made in NodeJS. The recording of the video calls is made through OpenTok and the audio is processed and transcribed by
Google Cloud Speech to Text API.

(Production of this app was made in an experimental setting).

## Getting Started

1. Clone or fork the master branch.
2. Create a free trial Account with Google Speech to Text API.
3. Create a free trial Account with OpenTok / TokBox.
4. Donwload the XAMPP.
5. Get Project API Key and Project Secret from your OpenTok Project.
6. Navigate to the backend folder. Open app.js and main.js
7. Replace the variables apiKey and projectSecret on line 10 and 11. In both files.
8. Navigate to Googles Speech to Text API. Create a Service Account Key in json format.
9. Replace the transcribingTesting-a9cd842add1a.json file inside the backend folder with your new donwloaded file.
10. Start XAMPP Control Panel.
11. Start Apache and MySQL.
11. Click on Admin for MySQL. Here you import the file eyeoda.sql in the root directory of this repository.

### Prerequisites

#### Frontend Dependencies

```
"@opentok/client": "^2.15.10"
"opentok-react": "^0.9.0"
"react": "^16.8.4"
"react-dom": "^16.8.4"
"react-scripts": "2.1.8"
```
#### Backend Dependencies

```
"@google-cloud/speech": "^2.3.1"
"body-parser": "^1.19.0"
"express": "^4.16.4"
"http": "0.0.0"
"mysql": "^2.17.1"
"opentok": "^2.8.1"
```

### Installing - Running the app on localhost.

* Make sure Apache and MySQL are running in the XAMPP Control Panel.
* Navigate to the backend folder in the terminal and type these commands:

```
set GOOGLE_APPLICATION_CREDENTIALS=transcribingTesting-a9cd842add1a.json
npm install
nodemon app
OR
node app.js
```
* Navigate to the src folder inside the frontend folder and open the variables.js file.

* Make sure line 7 and 8 reads:

```
// const API = 'https://donut.kjeld.io/server/';
const API = 'http://localhost:3010/server/';
```
* Navigate to the frontend folder in the terminal and type these commands:

```
npm install
npm start
```
* Project will start on localhost:3000.

## Deployment

Hosted on a Digital Ocean, VPS ubunto 18.04. For access to the live server or more informaton about deployment please contact on of the authors.

## Built With

* [ReactJS](https://reactjs.org/) - The Frontend framework
* [NodeJS](https://nodejs.org) - The Backend
* [NPM](https://www.npmjs.com/) - Dependency Management
* [WebRTC](https://webrtc.org/) - Video in browser
* [OpenTok](https://tokbox.com/a) - For video session creation
* [Google API](https://cloud.google.com/speech-to-text/docs/) - The Speech to text API.
* [DigitalOcean](https://www.digitalocean.com/) - Hosting


## Authors

* **Simon Gribert** - [Bitbucket Account](https://bitbucket.org/%7B58fa521a-794c-4b0e-8e26-bec158f5d6bd%7D/)


## Acknowledgments

* Peder Rotkirch - For inspiration and passion for the project and also idea inventor.
* Syed Naseh - Providing us with constant support and help with backend APIs. The joy he shows in his work is truly inspiring (even when sick).
