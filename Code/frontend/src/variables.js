import React from 'react';

import Welcome from './components/Welcome';
import Call from './components/Call';
import Transcription from './components/Transcription';

const API = 'https://donut.kjeld.io/server/';
// const API = 'http://localhost:3010/server/';

const PAGE = (appState, createSession, setName, changePage, joinSession) =>
({
  welcome: <Welcome
    appState={appState}
    createSession={createSession}
    setName={setName}
    joinSession={joinSession}
  />,
  call: <Call
    appState={appState}
    changePage={changePage}
    api={API}
  />,
  transcription: <Transcription
    appState={appState}
    api={API}
  />
});

const variables = {
  Page: PAGE,
  api: API
};

export default variables;
