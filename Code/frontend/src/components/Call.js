import React from 'react';
import { OTPublisher, OTSubscriber, createSession } from 'opentok-react';
import notRecording from './../img/notRecording.png';
import endCallImg from './../img/endcall.png';
import recording from './../img/recording.png';

class Call extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      streams: [],
      subConnected: false,
      archiveId: null,
      recording: false
    };

    this.sessionHelper = createSession({
      apiKey: this.props.appState.session.apiKey,
      sessionId: this.props.appState.session.sessionId,
      token: this.props.appState.session.token,
      onStreamsUpdated: streams => {
        this.setState({ streams: streams, subConnected: true});
      }
    });

    this.publisherEventHandlers = {
      streamCreated: event => {
        console.log('Publisher Stream Created...');
      },
      streamDestroyed: event => {
        console.log('Publisher Stream Destroyed...');
      }
    };

    this.subscriberEventHandlers = {
      connected: event => {
        console.log('Subscriber connected...');
      },
      destroyed: event => {
        console.log('Subscriber Disconnected...');
        this.endCall();
      }
    };

    this.startRecording = this.startRecording.bind(this);
    this.timer = this.timer.bind(this);
    this.stopRecording = this.stopRecording.bind(this);
    this.deleteSession = this.deleteSession.bind(this);
    this.endCall = this.endCall.bind(this);
  }

  componentWillUnmount() {
    this.sessionHelper.disconnect();
  }

  render(){
    return(
      <div>
        <OTPublisher
          session={this.sessionHelper.session}
          eventHandlers={this.publisherEventHandlers}
          properties={{
            publishVideo: true,
          }}
        />
        {this.state.subConnected ?
          this.state.streams.map(stream => {
            return (
              <OTSubscriber
                key={stream.id}
                session={this.sessionHelper.session}
                stream={stream}
                properties={{
                  width: '100%',
                  height: '100%'
                }}
                eventHandlers={this.subscriberEventHandlers}
              />
            );
        }) : null}
        <div className="call-btn" >
          <img onClick={this.startRecording} src={this.state.recording ? recording : notRecording} alt="Start Recording"/>
        </div>
        <div className="call-btn">
          <img onClick={this.deleteSession} src={endCallImg} alt="End Call"/>
        </div>
      </div>
    );
  }

  startRecording(){
    let payload = {
      sessionId: this.props.appState.session.sessionId
    };
    fetch(`${this.props.api}startRecording`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(payload)
    })
    .then(res => res.json())
    .then(data => {
      console.log(data.message);
      this.setState({archiveId: data.archiveId, recording: true});
      this.timer();
    })
    .catch(err => console.log(`Error: ${err.message}`));
  }

  stopRecording(){
    var payload = {
      archiveId: this.state.archiveId
    };
    fetch(`${this.props.api}stopRecording`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(payload)
    })
    .then(res => res.json())
    .then(data => {
      console.log(data.message);
      this.startRecording();
    })
    .catch(err => console.log(`Error: ${err.message}`));
  }

  deleteSession() {
    var payload = {
      sessionId: this.props.appState.session.sessionId
    };
    fetch(`${this.props.api}deleteSession`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(payload)
    })
    .then(res => res.json() )
    .then(data => {
      console.log(data.message);
      this.endCall();
    })
    .catch(err => console.log(`Error: ${err.message}`));
  }

  timer(){
    setTimeout(() => {
      this.stopRecording();
    }, 40000);
  }

  endCall() {
    this.props.changePage('transcription');
  }
}

export default Call;
