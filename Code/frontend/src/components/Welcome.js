import React from 'react';

class Welcome extends React.Component{
  render(){
    return (
      <div className="welcome">
        <h1>Welcome</h1>
        <div className="form">
          <div className="input-container">
            <label htmlFor="name">Nickname</label>
            <input id="name" type="text" value={this.props.appState.name} onChange={this.props.setName} />
          </div>
          <button onClick={this.props.createSession} className="welcome-btn">Create Call</button>
          {this.props.appState.currentSessions.length !== 0 ?
            this.props.appState.currentSessions.map((sessionId, index) => {
              return (
                <button key={index} className="welcome-btn"
                        onClick={() => this.props.joinSession(sessionId)}>Join {index}</button>
              )
            })
            : null}
        </div>
      </div>
    )
  }
}

export default Welcome;
