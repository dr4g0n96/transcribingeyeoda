import React from 'react';
import variable from './../variables.js';

class App extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 'welcome',
      name: '',
      currentSessions: []
    };

    this.changePage = this.changePage.bind(this);
    this.setName = this.setName.bind(this);
    this.createSession = this.createSession.bind(this);
    this.getSessions = this.getSessions.bind(this);
    this.joinSession = this.joinSession.bind(this);
  }

  componentDidMount() {
    this.getSessions();
  }

  render(){
    return (
      <div className="app">
        {variable.Page(
          this.state,
          this.createSession,
          this.setName,
          this.changePage,
          this.joinSession
        )[this.state.currentPage]}
      </div>
    )
  }

  createSession() {
    let payload = {
      name: this.state.name
    };
    fetch(`${variable.api}createSession`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(payload)
    })
    .then(res => res.json() )
    .then(data => {
      this.setState({session: data});
      this.changePage('call');
    })
    .catch(err => console.log(`Error: ${err.message}`));
  }

  getSessions() {
    fetch(`${variable.api}currentSessions`, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json() )
    .then(data => {
      this.setState({currentSessions: Object.keys(data)});
    })
    .catch(err => console.log(`Error: ${err.message}`));
  }

  joinSession(sessionId) {
    let payload = {
      name: this.state.name,
      sessionId: sessionId
    };
    fetch(`${variable.api}joinSession`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(payload)
    })
    .then(res => res.json() )
    .then(data => {
      this.setState({session: data});
      this.changePage('call');
    })
    .catch(err => console.log(`Error: ${err.message}`));
  }

  setName(event) {
    this.setState({name: event.target.value});
  }
  changePage(newPage) {
    this.setState({currentPage: newPage});
  }
}

export default App;
