import React from 'react';

class Transcription extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      transcription: ''
    };

    this.getTranscription = this.getTranscription.bind(this);
  }

  componentDidMount() {
    setInterval(() => {
      this.getTranscription();
    }, 30000);
  }

  render(){
    return (
      <div className="transcription">
        <h1>Transcription</h1>
        <div className="transcription-container">
          <p>{this.state.transcription}</p>
        </div>
      </div>
    )
  }

  getTranscription() {
    let payload = {
      sessionId: this.props.appState.session.sessionId
    };
    fetch(`${this.props.api}transcription`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(payload)
    })
    .then(res => res.json() )
    .then(data => {
      this.setState({transcription: ''});
      for (let i = 0; i < data.transcription.length; i++) {
        let newTranscription = this.state.transcription + data.transcription[i];
        this.setState({transcription: newTranscription });
      }
    })
    .catch(err => console.log(`Error: ${err.message}`));
  }
}

export default Transcription;
