import React from 'react';
import ReactDOM from 'react-dom';
import './app.css'
import '@opentok/client'
import App from './components/App'

ReactDOM.render(
    <App/>, document.querySelector('#root')
);
