//VARIABLES
const Opentok = require('opentok'),
      express = require('express'),
      bodyParser = require('body-parser'),
      mysql = require('mysql'),
      fs = require('fs'),
      http = require('http'),
      spawn = require('child_process').spawn,
      speech = require('@google-cloud/speech').v1p1beta1,
      apiKey = '46319222',
      projectSecret = '81539db7cd916e5b07be499c2a3fd3cab494223e'
      app = express(),
      opentok = new Opentok(apiKey,projectSecret);
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'eyeoda'
});
// const connection = mysql.createConnection({
//   host: '206.189.2.52',
//   user: 'eyeoda',
//   password: 'eyeoda123',
//   database: 'eyeoda'
// });
//INCLUDING PACKAGES IN THE API CALLS
app.use(express.json({
  type:['application/json','text/plain']
}));
app.set('sessions', {});
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//INIT SERVER AND CHECK FOR ARCHIVES
function init() {
  app.listen(3010, () => {
    console.log('Initial Startup of Server');
  });
  setInterval(() => {
    listArchives();
  }, 60000);
}
init();

//**************REST API CALLS*****************
//CREATING SESSION
app.post('/server/createSession', (req, res) => {
  opentok.createSession({mediaMode: 'routed'}, (err, session) => {
    if(err) return console.log(err);
    app.settings.sessions[session.sessionId] = session.sessionId;
    let tokenOptions= {
        data: `username=${req.body.name}`
    };
    token = opentok.generateToken(session.sessionId, tokenOptions);
    res.json({sessionId: session.sessionId, token: token, apiKey: apiKey});
  });
});

//JOIN ACTIVE SESSION
app.post('/server/joinSession', (req, res) => {
    let tokenOptions= {
        data: `username=${req.body.name}`
    };
    token = opentok.generateToken(req.body.sessionId, tokenOptions);
    res.json({sessionId: req.body.sessionId, token: token, apiKey: apiKey});
});

//GET CURRENT SESSIONS
app.get('/server/currentSessions', (req, res) => {
  res.status(200).send(app.settings.sessions);
});

//START ARCHIVING TO OPENTOK
app.post('/server/startRecording', (req,res) => {
  let archiveOpt = {
    name: 'eyeoda',
    hasVideo: false
  };
  opentok.startArchive(req.body.sessionId, archiveOpt, (err, archive) => {
    if(err) return console.log(err);
    console.log('New Archive: ', archive.id);
    res.json({message: 'Archiving Session...', archiveId: archive.id});
  });
});

//STOP ARCHIVING SELECTED SESSION
app.post('/server/stopRecording', (req, res) => {
  opentok.stopArchive(req.body.archiveId, (err, archive) => {
    if(err) return console.log(err);
    console.log('Stopped Archive: ', archive.id);
    res.json({message: 'Archiving Stopped...'});
  });
});

//DELETE SESSSION
app.post('/server/deleteSession', (req, res) => {
  delete app.settings.sessions[req.body.sessionId];
  res.json({message: 'Session Deleted...'});
});

//GET TRANSCRIPTIONS FOR A SPECIFIC SESSION
app.post('/server/transcription', (req, res) => {
  let transcriptionArr = [],
      query = 'SELECT ?? FROM ?? WHERE ?? = ?',
      table = ['transcription', 'transcriptions', 'session_id', req.body.sessionId];
  query = mysql.format(query,table);
  connection.query(query, (err, rows) => {
    for (var i = 0; i < rows.length; i++) {
      transcriptionArr.push(rows[i].transcription);
    }
    console.log(`Sent transcription for session ${req.body.sessionId}...`);
    res.json({transcription: transcriptionArr});
  });
});

//************************TRANSCRIBING ARCHIVES*************
//CHECK FOR NEW ARCHIVES
function listArchives() {
  opentok.listArchives((err, archives, totalCount) => {
    if(err) return console.log(err);
    if(totalCount > 0 && archives[archives.length - 1].status === 'available') {
      console.log('Selected Archive ID: ', archives[archives.length - 1].id);
      let httpUrl = archives[archives.length - 1].url.replace('https', 'http');
      downloadArchive(
        httpUrl,
        archives[archives.length - 1].id,
        archives[archives.length - 1].sessionId
      );
    }
  });
}

//DOWNLOAD SELECTED ARCHIVE FROM OPENTOK
function downloadArchive(url, archiveId, sessionId) {
  const file = fs.createWriteStream(`${archiveId}.mp4`);
  http.get(url, (res) => {
    res.pipe(file);
    res.on('end', () => {
      console.log('Download Complete...');
      const stats = fs.statSync(`${archiveId}.mp4`);
      const fileSizeInBytes = stats.size;
      fileSizeInBytes !== 0 ?
        convertFile(archiveId, sessionId) :
        deleteArchives(archiveId);
    });
  });
}

//CONVERT AUDIO FILE FROM MP4 TO FLAC
function convertFile(archiveId, sessionId) {
  const fileName = `./${archiveId}.mp4`;
  let fileNameFlac = `${archiveId}.flac`;
  let ffmpeg = spawn('ffmpeg', ['-y', '-i', `${fileName}`, `${fileNameFlac}`]);
  ffmpeg.on('exit', (statusCode) => {
    if(statusCode === 0) {
      console.log('Conversion Successful...');
      transcribe(archiveId, sessionId).catch(console.error);
    }
  });
}

//TRANSCRIBE FLAC FILE
async function transcribe(archiveId, sessionId) {
  const client = new speech.SpeechClient();
  let fileNameFlac = `${archiveId}.flac`;
  const files = fs.readFileSync(fileNameFlac);
  const audioBytes = files.toString('base64');
  const audio = {
    content: audioBytes
  };
  const config = {
    encoding: 'FLAC',
    sampleRateHertz: 48000,
    languageCode: 'sv-SE',
    enableAutomaticPunctuation: true
  };
  const request = {
    audio: audio,
    config: config
  };
  const [response] = await client.recognize(request);
  const transcription = response.results
    .map(result => result.alternatives[0].transcript)
    .join('\n');
  console.log(`Transcription: ${transcription}`);
  saveTranscription(archiveId, transcription, sessionId);
}

//INSERT TRANSCRIPTION, SESSION ID, ARCHIVE ID INTO DATABASE
function saveTranscription(archiveId, transcription, sessionId) {
  let query = 'INSERT INTO ??(??,??,??) VALUES (?,?,?)',
      inserts = ['transcriptions',
                 'session_id', 'archive_id', 'transcription',
                  sessionId, archiveId, transcription];
  query = mysql.format(query, inserts);
  connection.query(query, (err, rows) => {
    err ? console.log('Could not reach database...') :
          console.log('Insertion complete...'),
          deleteArchives(archiveId);
  });
}

//DELETE TRANSCRIBED ARCHIVE
function deleteArchives(archiveId) {
  opentok.deleteArchive(archiveId, err => {
    if(err) return console.log(err);
  });
  fs.unlink(`${archiveId}.mp4`, err => {
    if(err) return console.log(err);
    console.log(`Deleted ${archiveId}.mp4...`);
  });
  fs.unlink(`${archiveId}.flac`, err => {
    if(err) return console.log(err);
    console.log(`Deleted ${archiveId}.flac...`);
  });
}
