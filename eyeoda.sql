-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Värd: 127.0.0.1
-- Tid vid skapande: 10 maj 2019 kl 21:58
-- Serverversion: 10.1.36-MariaDB
-- PHP-version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databas: `eyeoda`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `transcriptions`
--

CREATE TABLE `transcriptions` (
  `id` int(128) NOT NULL,
  `session_id` varchar(256) NOT NULL,
  `archive_id` varchar(128) NOT NULL,
  `transcription` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `transcriptions`
--

INSERT INTO `transcriptions` (`id`, `session_id`, `archive_id`, `transcription`) VALUES
(37, '2_MX40NjMxOTIyMn5-MTU1NzQxNzIzNTY5Mn5MaEJLMDdXaDl0SE5Ydk1kQkE5MUZRUk1-fg', 'be962f92-9b44-4e41-af95-3c10f4d7dabd', 'Tjena tjena. Jag hoppas att det här funkar inte konstigt. Då pratar vi med att lyssna på sig själv jättekonstigt. Jag stannar ut på ord ord än de som är Na samtidigt som jag pratar om dig. Gör mig förvirrad fan konstigt.'),
(38, '2_MX40NjMxOTIyMn5-MTU1NzQxNzIzNTY5Mn5MaEJLMDdXaDl0SE5Ydk1kQkE5MUZRUk1-fg', '28b3a46f-7261-47ba-bc78-0c4a0187a223', ''),
(39, '1_MX40NjMxOTIyMn5-MTU1NzQyODE5NDAwNn40U0ZqUTFmZER4Wi9qYlVVbFVVRFJkeS9-fg', 'fc0f5235-d07e-4cfd-9029-bdd20813710a', 'åka Jag tror att du borde ha börjat men nu och jag hoppas att det här kommer att fungera än så länge verkar det som att allting är okej Så kommer lite sömn Och så det verkar som att det fungerar en länge katt hoppade ner från sängen men okej förhoppningsvis så ganska snart så kommer de att starta en ny are carrying'),
(40, '1_MX40NjMxOTIyMn5-MTU1NzQyODE5NDAwNn40U0ZqUTFmZER4Wi9qYlVVbFVVRFJkeS9-fg', '2a36bd2c-cae1-441a-8933-50304c91a7bd', 'Yes, där har vi en ny arkivering och förhoppningsvis så hoppar jag pratar tillräckligt länge för att det ska bli tre stycken av de här. Jag vill ju veta ifall att det går att klippa igenom som jag gjort nu så här i slutet så att säga så nu så fungerar alldeles utmärkt att prata här så du kan ju prata lite om vad som händer imorgon imorgon är att börja det här med en callback from the pentarchy NRK'),
(41, '1_MX40NjMxOTIyMn5-MTU1NzQyODE5NDAwNn40U0ZqUTFmZER4Wi9qYlVVbFVVRFJkeS9-fg', 'ae5a1366-da91-4c42-9bca-337bb885e831', 'Robo så så kommer det att fungera det bra att då kan vi göra men kolla på sig skulden next också. Vilket betyder att vi kommer inte vatten per minut för arkeving eller för att granska 2004 så blir det perfekt.'),
(42, '1_MX40NjMxOTIyMn5-MTU1NzQyODU0MDI1NH5KTHFBbGV6ZVdHR3Ywc1Y0YXFkTDVmVzd-fg', 'fe0f41da-3f5e-4a7a-9b0d-f8025bbc9ce8', 'Okej så då börjar det igen att Archive att jag slutade förra gången med att berätta att jag tänkte göra imorgon också skulle säga om det var att\n och det kan ju vara så att jag får ett jobb där så får du se om det går bra att det hade varit skitbra om det här kommer fungera.\n För jag hade kunnat bli så här att det hade ju varit super positivt så får ni se det så kommer det att gå alldeles utmärkt vilken sekund.'),
(43, '1_MX40NjMxOTIyMn5-MTU1NzQyODU0MDI1NH5KTHFBbGV6ZVdHR3Ywc1Y0YXFkTDVmVzd-fg', '4e5ccc32-fd4b-4b14-a2d0-f0a22961d861', 'Soppa och börja igen, så gör du just nu så det är bra. Men alltså det är så jag vill att du ska visa hur jag åt idag hamburgare 2 hamburgare med bröd nötkött lite rosa det lite sallad lite tomater tomater tomater och ost, Slottskällan visar en lök.'),
(44, '1_MX40NjMxOTIyMn5-MTU1NzQyODU0MDI1NH5KTHFBbGV6ZVdHR3Ywc1Y0YXFkTDVmVzd-fg', '4078d2ef-0597-4a8b-a080-a64fb8060032', 'perfekt'),
(45, '1_MX40NjMxOTIyMn5-MTU1NzQzMDE1NzUxMH5LRUVuZmhmSlBxNy9RNmh6a0FpZU1YdXV-fg', '2688fbd8-3216-448f-9b35-055ea1cf0302', 'Jag tror att det kommer funka nu får du så har du ju allting är väl fortsätta pratar I am not just nu så får du så kan ni göra det upp så här och så där funkar så titta på trean om inte annat så kanske från annat ord för att fått missfall ändå. Jag tror därför att det enda problemet som kan komma upp. Det är ifall vi får tomt.'),
(46, '1_MX40NjMxOTIyMn5-MTU1NzQzMDE1NzUxMH5LRUVuZmhmSlBxNy9RNmh6a0FpZU1YdXV-fg', '7b0d62c4-08a9-4e89-8f2a-ed85c22de4fe', 'Jag hoppas att imorgon vi kan faktiskt använda oss av opentok. Systems att det släpper där intervallerna hela tiden.\n Det var ju bra.\n Så fort så blir det klart ifall du skulle kunna fortsätta med det. Ja jag vet inte. Jag har inte så mycket mer att säga än så, alltså det är'),
(47, '1_MX40NjMxOTIyMn5-MTU1NzQzMDE1NzUxMH5LRUVuZmhmSlBxNy9RNmh6a0FpZU1YdXV-fg', '50d6f029-e238-4c1a-a01f-1a7e631392ca', 'Du ser ganska bra ut ändå. Jag tror att vi vi kommer snart ska vi kolla lite på stranden slut då istället kunskaper och föddes utan också Och nu lägger på.'),
(48, '2_MX40NjMxOTIyMn5-MTU1NzQ4MDgxMzM0OX5xZnBLWm56Qnowb2tqczM2RWc1UFU0NGR-fg', 'd9cf9063-57a0-469a-9314-41470f022f76', 'Att spela in National Jag hoppas att den här är konfigurera ok att kolla upp på datorn som annars så fult annat men ta det alltid om allting. Det här fungerar alldeles utmärkt. Då kommer vi att fixa till styling lite när man kommer hem kommer fixa lite. Sen så kommer vi fixa till att precis stylingen eller hur det ser ut på internet.'),
(49, '1_MX40NjMxOTIyMn5-MTU1NzUxMjQ5NTU1NH5FRlF0OWFRaVFlc1pOOW1pV0hOQ0kwVTN-fg', 'a4e77cb0-1445-49c2-bc8b-eeb8168a7420', 'Okej Låt mig prata lite nu då. Får se om det funkar. Jag hoppas jag att ifall det här Nu funkar som det ska att du skulle kunna ta emot två olika personer med två olika tavla och att det ska fungera på ett sätt som som det är tänkt att jag ska dit lite styling så det ser lite snyggare ut än vad det gjorde innan fått min fru så gör det regnar det ganska stor skillnad på liksom projekt.'),
(50, '1_MX40NjMxOTIyMn5-MTU1NzUxMjQ5NTU1NH5FRlF0OWFRaVFlc1pOOW1pV0hOQ0kwVTN-fg', 'b0692d13-84f9-48be-a43d-7fab33952121', 'Kommer något där de Sade inte har spenderat för mycket tid börjar Även om det var kul att visa något själv, men det ser bra ut och så blir det liksom så vi får titta på det. Sen så kör vi på bara skriva lite så.'),
(51, '2_MX40NjMxOTIyMn5-MTU1NzUxMzYxNTU3Nn51d2ZzOXk3ajdEenVPdW1pRXRVZlJiYVh-fg', '04bd1970-05d8-4f98-a3eb-88eba9069af8', ''),
(52, '1_MX40NjMxOTIyMn5-MTU1NzUxNDMwMzA4OX5iS3l6d2xyZThWSUpoQi9zZnN2cVV1Z0R-fg', '6e0bbadb-f4a1-43c7-b8bd-358ac2bc1b01', 'Tjena tjena tjena Idag är det fredag imorgon lördag. Jag Jacob kommer se en film som är det imorgon med min morsan så finns det skulle bli kul. Annars så ska vi äta hamburgare ganska kul. Jag måste tidigt städa du inte kött, men annars än det jag sa idag göra klart det här men jag måste gå och fiska med i snö titta på hockey.'),
(53, '1_MX40NjMxOTIyMn5-MTU1NzUxNDMwMzA4OX5iS3l6d2xyZThWSUpoQi9zZnN2cVV1Z0R-fg', '95b82958-7235-43cd-8826-1923bddac476', 'papper till glas'),
(54, '2_MX40NjMxOTIyMn5-MTU1NzUxNTI1MjkwMH5mNzRRWHdoWWhPSU9HcmJCUUpKMXo2QjR-fg', 'd7f0490d-1045-4c52-9e5e-40843f71ef9a', 'Channel Tag: 1 Tjena Tjena vi ska prova att snacka två olika personer. Då såg ju som sagt jag som en person person äter macka. Ja nu som jag kommer nu att anmäla njuta den här mycket så ska gå med Agneta Okej Så nu förhoppningsvis så blir det två olika personer Focaccia hoppas att det funkar om Därför hoppas verkligen att det vore helt sjukt men'),
(55, '2_MX40NjMxOTIyMn5-MTU1NzUxNTI1MjkwMH5mNzRRWHdoWWhPSU9HcmJCUUpKMXo2QjR-fg', '23ceba1e-8c4a-4e22-ad16-a6e6f41f9aa3', 'Channel Tag: 2 Men såklart så vet man inte det förrän det är liksom ja.');

--
-- Index för dumpade tabeller
--

--
-- Index för tabell `transcriptions`
--
ALTER TABLE `transcriptions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT för dumpade tabeller
--

--
-- AUTO_INCREMENT för tabell `transcriptions`
--
ALTER TABLE `transcriptions`
  MODIFY `id` int(128) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
